//
//  StackView.swift
//  StackView
//
//  Created by Josh Campion on 05/01/2016.
//  Copyright © 2016 The Distance. All rights reserved.
//

import Foundation

public func CreateStackView(_ arrangedSubviews:[UIView]) -> UIStackView {
    return UIStackView(arrangedSubviews: arrangedSubviews)
}


