//
//  SwitchStack.swift
//  Pods
//
//  Created by Josh Campion on 10/02/2016.
//
//

import Foundation

open class SwitchStack: ErrorStack, ValueElement {
    
    public let titleLabel:UILabel
    public let subtitleLabel:UILabel
    
    public let switchControl:UISwitch
    
    open var validation:Validation<Bool>?
    
    public init(switchControl:UISwitch,
        titleLabel:UILabel = UILabel(),
        subtitleLabel:UILabel = UILabel(),
        errorLabel:UILabel = UILabel(),
        errorImageView:UIImageView = UIImageView(),
        iconImageView:UIImageView = UIImageView()) {
            
            self.switchControl = switchControl
            
            self.titleLabel = titleLabel
            self.subtitleLabel = subtitleLabel
            
            let textStack = CreateStackView([self.titleLabel, self.subtitleLabel])
            textStack.axis = .vertical
            textStack.spacing = 4.0
            
            let switchStack = CreateStackView([textStack, self.switchControl])
            switchStack.axis = .horizontal
            switchStack.spacing = 8.0
            switchStack.alignment = .center
            
            super.init(centerComponent: switchStack,
                errorLabel: errorLabel,
                errorImageView: errorImageView,
                iconImageView: iconImageView)
    }
    
    open func getValue() -> Any? {
        return switchControl.isOn
    }
    
    open func setValue<T>(_ value: T?) -> Bool {
        
        if let on = value as? Bool {
            switchControl.isOn = on
            return true
        } else {
            return false
        }
    }
    
    open func validateValue() -> ValidationResult {
        return validation?.validate(switchControl.isOn) ?? .valid
    }
}
