//
//  UIPickerDataControllers.swift
//  StackView
//
//  Created by Josh Campion on 13/09/2015.
//  Copyright © 2016 Josh Campion.
//

import Foundation

public enum UIPickerType {
    case date(UIDatePickerDataController)
    case picker(UIPickerViewDataController)
}

open class UIDatePickerDataController: NSObject {
    
    open var dateFormatter:DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeStyle = DateFormatter.Style.none
        formatter.dateStyle = DateFormatter.Style.medium
        
        return formatter
        }()
    
    public let datePicker: UIDatePicker
    public let textField: UITextField
    
    public init(datePicker:UIDatePicker, textField:UITextField) {
        
        self.datePicker = datePicker
        self.textField = textField
        
        super.init()
        
        if let text = textField.text,
            let date = dateFormatter.date(from: text) {
                datePicker.date = date
        }
        
        self.textField.inputView = datePicker
        
        datePicker.addObserver(self, forKeyPath: "date", options: [.old, .new], context: nil)
        datePicker.addTarget(self, action:#selector(UIDatePickerDataController.dateChanged(_:)), for: .valueChanged)
    }
    
    deinit {
        datePicker.removeObserver(self, forKeyPath: "date")
    }
    
    override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let datePicker = object as? UIDatePicker, keyPath == "date" {
            dateChanged(datePicker)
        }
    }
    
    @objc open func dateChanged(_ sender:UIDatePicker) {
        
        textField.text = dateFormatter.string(from: sender.date)
    }
}

open class UIPickerViewDataController:NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
    
    public let choices:[[String]]
    public let pickerView:UIPickerView
    public let textField:UITextField
    
    public init(choices:[[String]], pickerView:UIPickerView, textField:UITextField) {
        self.choices = choices
        self.pickerView = pickerView
        self.textField = textField
        
        super.init()
        
        pickerView.dataSource = self
        pickerView.delegate = self
        
        textField.inputView = pickerView
        
        NotificationCenter.default.addObserver(self, selector: #selector(UIPickerViewDataController.matchPickerToTextField), name: UITextField.textDidBeginEditingNotification, object: textField)
    }
    
    @objc open func matchPickerToTextField() {
        if let text = textField.text,
            let idx = choices[0].index(of: text) {
                pickerView.selectRow(idx + 1, inComponent: 0, animated: true)
        }
    }
    
    open func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return choices.count
    }
    
    open func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return choices[component].count + 1
    }
    
    open func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        if row == 0 {
            return NSAttributedString(string: "")
        }
        
        return NSMutableAttributedString(string: choices[component][row - 1], attributes: [NSAttributedString.Key.font: textField.font!])
    }
    
    open func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row > 0 {
            textField.text = choices[component][row - 1]
        } else {
            textField.text = nil
        }
    }
}
