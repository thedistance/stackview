//
//  FormValidation.swift
//  CreatedStack
//
//  Created by Josh Campion on 23/10/2015.
//

import Foundation
import TheDistanceCore

public enum ValidationResult: Equatable {
    case valid
    case invalid(reason:String)
}

public func ==(v1:ValidationResult, v2:ValidationResult) -> Bool {
    switch (v1, v2) {
    case (.valid, .valid):
        return true
    case (.valid, .invalid(_)), (.invalid(_), .valid):
        return false
    case (.invalid(let m1), .invalid(let m2)):
        return m1 == m2
    }
}

/// Combines two `ValidationResult`s as if combining two `Bool`s using `&&`. The invalidation messages are combined with "\n".

public func &&(v1:ValidationResult, v2:ValidationResult) -> ValidationResult {
    switch (v1, v2) {
    case (.valid, .valid):
        return .valid
    case (.valid, .invalid(_)):
        return v2
    case (.invalid(_), .valid):
        return v1
    case (.invalid(let m1), .invalid(let m2)):
        return .invalid(reason: m1 + "\n" + m2)
    }
}

/// Combines two `ValidationResult`s as if combining two `Bool`s using `||`. If both parameters are `Invalid`, invalidation messages are combined with "\n".

public func ||(v1:ValidationResult, v2:ValidationResult) -> ValidationResult {
    switch (v1, v2) {
    case (.valid, .valid), (.valid, .invalid(_)), (.invalid(_), .valid):
        return .valid
    case (.invalid(let m1), .invalid(let m2)):
        return .invalid(reason: m1 + "\n" + m2)
    }
}

/**

Structure that performs validation on a value of a given type, returning `true` or `false` if that validation passes.

- seealso: NullStringValidation, EmailValidation

*/
public struct Validation<Type> {
    
    /// Should return `true` if the value passes, `false` otherwise. The value is optional as user data is likely to contain `nil` entries.
    public let validate:(_ value:Type?) -> ValidationResult
    
    /// Initialiser assigning the parameters to the properties of the same names.
    public init(message:String, validation:@escaping (_ value:Type?) -> Bool) {
        // self.message = message
        self.validate = { (v:Type?) -> ValidationResult in
            return validation(v) ? .valid : .invalid(reason: message)
        }
    }
    
    public init(andValidations:[Validation<Type>], message:String? = nil) {
        self.validate = { (v:Type?) -> ValidationResult in
            
            let compound = andValidations.reduce(.valid, { $0 && $1.validate(v) })
            
            if let m = message, compound != .valid {
                return .invalid(reason: m)
            } else {
                return compound
            }
        }
    }
    
    public init(orValidations:[Validation<Type>], message:String? = nil) {
        
        self.validate = { (v:Type?) -> ValidationResult in
            let compound = orValidations.reduce(.valid, { $0 || $1.validate(v) })
            
            if let m = message, compound != .valid {
                return .invalid(reason: m)
            } else {
                return compound
            }
        }
    }
}

/// Convenience creator for a validation that checks whether a given string, trimmed from whitespace, is has content.
public func NonEmptyStringValidation(_ message:String) -> Validation<String> {
    
    return Validation<String>(message: message, validation: { (value) -> Bool in
        
        if let str = value {
            return !str.whitespaceTrimmedString().isEmpty
        }
        
        return false
    })
}

/**

 Convenience creator for a validation that checks whether a given string is a valid email. The check is based on the regex:

     [a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+
 
 - parameter message: The `message` property of the returned `Validation`.
 - parameter allowingNull: Whether or not an empty string will be allowed. Default is `false`. `true` allows validation of an optional email address.
*/
public func EmailValidationWithMessage(_ message:String, allowingNull:Bool = false) -> Validation<String> {
    
    return Validation<String>(message: message, validation: { (value) -> Bool in
        
        let nullValidation = NonEmptyStringValidation("")
        
        guard let stringValue = value else {
            return false
        }
        
        if !allowingNull && nullValidation.validate(stringValue) != ValidationResult.valid {
            return false
        }
        
        if allowingNull && stringValue.isEmpty {
            return true
        }
        
        var regexString = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}"
        regexString += "\\@"
        regexString += "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}"
        regexString += "("
        regexString += "\\."
        regexString += "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}"
        regexString += ")+"
        
        // this is a programmer error so ensure it is correct by force
        let regex = try! NSRegularExpression(pattern: regexString, options: .caseInsensitive)
        return regex.matches(in: stringValue, options: NSRegularExpression.MatchingOptions.reportProgress, range: NSMakeRange(0, stringValue.count)).count > 0
    })
}

/**

 Convenience creator for a validation that checks whether a given string, trimmed from whitespace is a valid phone number. The check is based on the regex:

    [\\+]?[0-9.-]+

 - parameter message: The `message` property of the returned `Validation`.
 - parameter allowingNull: Whether or not an empty string will be allowed. Default is `false`. `true` allows validation of an optional phone number.
*/
public func PhoneValidationWithMessage(_ message:String, allowingNull:Bool = false) -> Validation<String> {
    
    return Validation<String>(message: message, validation: { (value) -> Bool in
        let nullValidation = NonEmptyStringValidation("")
        
        guard let stringValue = value else {
            return false
        }
        
        if !allowingNull && nullValidation.validate(stringValue) == .valid {
            return false
        }
        
        if allowingNull && stringValue.isEmpty {
            return true
        }
        
        
        let regexString = "[\\+]?[0-9.-]+"
        
        // this is a programmer error so ensure it is correct by force
        let regex = try! NSRegularExpression(pattern: regexString, options: .caseInsensitive)
        return regex.matches(in: stringValue, options: NSRegularExpression.MatchingOptions.reportProgress, range: NSMakeRange(0, stringValue.count)).count > 0
    })
}
