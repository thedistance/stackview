//
//  ErrorStack.swift
//  StackView
//
//  Created by Josh Campion on 09/02/2016.
//


import Foundation
import TheDistanceCore
import UIKit

open class ErrorStack:CreatedStack {
    
    // MARK: Errors
    
    /// Sets the text of the `errorLabel` and shows / hides `errorLabel` and `errorImageView` based on whether this string `.isEmpty`. This can be configured manually or setting `self.validation` and calling `validateValue()`.
    open var errorText:String? {
        didSet {
            errorLabel.text = errorText
            errorLabel.isHidden = errorText?.isEmpty ?? true
            errorImageView.isHidden = errorLabel.isHidden
            
            // TODO: Weak link to TextResponder Cocoapod
            // this will cause layout to occur which could affect the position of other text input items, after which the keyboard responder should be notified to update the scroll accordingly.
            DispatchQueue.main.async { () -> Void in
                // NSNotificationCenter.defaultCenter().postNotificationName(KeyboardResponderRequestUpdateScrollNotification, object: nil)
            }
        }
    }
    
    /// The label showing the error associated with the user's input. The text for this label should be set through the `errorText` property, which configures properties such as showing and hiding the label.
    public let errorLabel:UILabel
    
    /// The `UIImageView` to show an error icon centered on the text input if there is an error.
    public let errorImageView:UIImageView
    
    /// The `UIImageView` to show an icon aligned to the left of the text input.
    public let iconImageView:UIImageView
    
    /// A horizontal `StackView` containing the `errorImageView` and `centerComponent` from the default initialiser
    open var centerStack: UIStackView
    
    /// A vertical `StackView` containing the `centerStack` and `errorLabel`.
    open var errorStack: UIStackView
    
    public init(centerComponent:UIView,
        errorLabel:UILabel = UILabel(),
        errorImageView:UIImageView = UIImageView(),
        iconImageView:UIImageView = UIImageView()) {
            
            self.errorLabel = errorLabel
            self.errorImageView = errorImageView
            self.iconImageView = iconImageView
            
            errorLabel.numberOfLines = 0
            errorLabel.isHidden = true
            errorLabel.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.caption1)
            
            for iv in [errorImageView, iconImageView] {
                
                iv.contentMode = .scaleAspectFit
                iv.backgroundColor = UIColor.clear
                iv.isHidden = true
                
                iv.setContentHuggingPriority(UILayoutPriority(255.0), for: .horizontal)
                
                iv.setContentCompressionResistancePriority(UILayoutPriority(760.0), for: .horizontal)
                iv.setContentCompressionResistancePriority(UILayoutPriority(760.0), for: .vertical)
            }
            
            centerStack = CreateStackView([centerComponent, errorImageView])
            centerStack.axis = .horizontal
            centerStack.spacing = 8.0
            
            errorStack = CreateStackView([centerStack, errorLabel])
            errorStack.axis = NSLayoutConstraint.Axis.vertical
            errorStack.alignment = .fill
            errorStack.distribution = .fill
            errorStack.spacing = 8.0
            
            super.init(arrangedSubviews: [iconImageView, errorStack])
            stack.axis = .horizontal
            stack.spacing = 8.0
    }
}
