Pod::Spec.new do |s| 
  s.name         = "TDStackView"
  s.version      = "1.1"
  s.summary      = "Use stack views"
  s.homepage     = "https://github.com/thedistance"
  s.license      = "MIT"
  s.author       = { "The Distance" => "dev@thedistance.co.uk" }
  s.platform     = :ios
  s.source       = { :git => "https://bitbucket.org/thedistance/stackview.git", :tag => "#{s.version}" }
  s.swift_version = '4.2'

  s.ios.deployment_target = '9.0'

  #s.source_files = 'TDStackView/Classes/**/*.swift', 'TDStackView/Extensions/**/*.swift', 'TDStackView/Protocols/**/*.swift'
  
  s.requires_arc = true

  s.subspec 'Core' do |ssc|
	ssc.source_files = 'TDStackView/Classes/**/*.swift', 'TDStackView/Extensions/**/*.swift', 'TDStackView/Protocols/**/*.swift'
  	ssc.dependency 'TheDistanceCore'
    ssc.dependency 'KeyboardResponder'
  end

  #s.subspec 'Photos' do |ssp|

    #ssp.source_files = 'TDStackView/Photos & Videos/**/*.swift'
    #ssp.resource_bundles = { 'TDStackViewPhotos' =>  'TDStackView/Photos & Videos/Resources/**/*.png'}

    #ssp.dependency 'TDViperKit'
    #ssp.dependency 'AdvancedOperationKit'
    #ssp.dependency 'TDStackView/Core'
    #ssp.framework = 'MobileCoreServices'
  #end
  
  
end
